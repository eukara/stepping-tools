#import <Foundation/Foundation.h>
#import <AppKit/AppKit.h>

int main(int argc, char **argv)
{
	int ch;
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	NSApplication *application = [NSApplication sharedApplication];
	NSAlert *alertPanel = [[NSAlert  alloc] init];
	NSString *alertTitle = nil; 
	NSString *alertInfo = nil; 
	NSString *alertButtonText = nil; 
	NSString *alertIconPath = nil;
	NSInteger alertStyle = 0;

	while ((ch = getopt(argc, argv, "htabis")) != -1) {
		switch (ch) {
		case 'h':
			fputs( "-h help\n", stderr );
			fputs( "-t title\n", stderr );
			fputs( "-a alert text\n", stderr );
			fputs( "-b add button with text\n", stderr );
			fputs( "-i path to alert icon\n", stderr );
			fputs( "-s 0 (alert) 1 (info) 2 (critical)\n", stderr );
			return 0;
		case 't':
			alertTitle = [NSString stringWithUTF8String: argv[optind]];
			optind++;
			break;
		case 'a':
			alertInfo = [NSString stringWithUTF8String: argv[optind]];
			optind++;
			break;
		case 'b':
			alertButtonText = [NSString stringWithUTF8String: argv[optind]];
			[alertPanel addButtonWithTitle: alertButtonText];
			optind++;
			break;
		case 'i':
			alertIconPath = [NSString stringWithUTF8String: argv[optind]];
			optind++;
			break;
		case 's':
			alertStyle = (NSInteger)atoi(argv[optind]);
			break;
		default:
			return 1;
		}
	}
	argc -= optind;
	argv += optind;

	/* bounds check */
	if (alertStyle < NSWarningAlertStyle || alertStyle > NSCriticalAlertStyle)
		alertStyle = 0;

	/* options */
	if (alertTitle)
		[alertPanel setMessageText: alertTitle];
	if (alertInfo)
		[alertPanel setInformativeText: alertInfo];

	[alertPanel setAlertStyle: (NSAlertStyle)alertStyle];

	if (alertIconPath != nil) {
		NSImage *iconImage = [[NSImage alloc] initWithContentsOfFile: alertIconPath];
		[alertPanel setIcon: iconImage];
		[iconImage release];
	}

	/* run the panel */
	switch ([alertPanel runModal]) {
	case NSAlertFirstButtonReturn:
		fputs( "1", stdout );
		break;
	case NSAlertSecondButtonReturn:
		fputs( "2", stdout );
		break;
	case NSAlertThirdButtonReturn:
		fputs( "3", stdout );
		break;
	}

	[alertPanel release];
	[application stop: nil];
	[pool drain];

	return 0;
}