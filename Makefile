# Makefile for GS-Tools

GS_FLAGS=$(shell gnustep-config --objc-flags)
GS_LIBS=$(shell gnustep-config --gui-libs)

TOOLS=NSAlertPanel NSOpenPanel NSSavePanel

all:
	$(MAKE) clean
	$(MAKE) $(TOOLS)

NSAlertPanel:
	$(CC) $(GS_FLAGS) NSAlertPanel.m $(GS_LIBS) -o NSAlertPanel

NSOpenPanel:
	$(CC) $(GS_FLAGS) NSOpenPanel.m $(GS_LIBS) -o NSOpenPanel

NSSavePanel:
	$(CC) $(GS_FLAGS) NSSavePanel.m $(GS_LIBS) -o NSSavePanel

clean:
	rm -v $(TOOLS) *.d 

install:
	cp -v $(TOOLS) ~/Library/Tools