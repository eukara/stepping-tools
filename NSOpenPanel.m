#import <Foundation/Foundation.h>
#import <AppKit/AppKit.h>

int main(int argc, char **argv)
{
	int ch;
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	NSApplication *application = [NSApplication sharedApplication];
	NSMutableArray *fileTypes = nil;
	NSOpenPanel *openPanel = nil;
	NSString *panelTitle = nil; 
	NSString *panelPrompt = nil; 
	NSString *appIconPath = nil;
	BOOL mulSel = NO;
	BOOL dirSel = YES;
	BOOL filSel = YES;

	while ((ch = getopt(argc, argv, "htpmdfia")) != -1) {
		switch (ch) {
		case 'h':
			fputs( "-h help\n", stderr );
			fputs( "-t title\n", stderr );
			fputs( "-p prompt\n", stderr );
			fputs( "-m 0/1 multiple selection\n", stderr );
			fputs( "-d 0/1 directory selection\n", stderr );
			fputs( "-f 0/1 file selection\n", stderr );
			fputs( "-i icon\n", stderr );
			fputs( "-a add file type\n", stderr );
			return 0;
			break;
		case 't':
			panelTitle = [NSString stringWithUTF8String: argv[optind]];
			optind++;
			break;
		case 'p':
			panelPrompt = [NSString stringWithUTF8String: argv[optind]];
			optind++;
			break;
		case 'm':
			mulSel = atoi(argv[optind]) ? YES : NO;
			optind++;
			break;
		case 'd':
			dirSel = atoi(argv[optind])? YES : NO;
			optind++;
			break;
		case 'f':
			filSel = atoi(argv[optind])? YES : NO;
			optind++;
			break;
		case 'i':
			appIconPath = [NSString stringWithUTF8String: argv[optind]];
			optind++;
			break;
		case 'a':
			if (fileTypes == nil)
				fileTypes = [NSMutableArray new];

			[fileTypes addObject: [NSString stringWithUTF8String: argv[optind]]];
			optind++;
			break;
		default:
			return 1;
		}
	}
	argc -= optind;
	argv += optind;

	/* icon */
	if (appIconPath != nil) {
		NSImage *iconImage = [[NSImage alloc] initWithContentsOfFile: appIconPath];
		[application setApplicationIconImage: iconImage];
		[iconImage release];
	}

	/* needs to be inited after our app icon changes */
	openPanel = [NSOpenPanel openPanel];

	/* options */
	[openPanel setAllowsMultipleSelection: mulSel];
	[openPanel setCanChooseDirectories: dirSel];
	[openPanel setCanChooseFiles: filSel];

	if (panelTitle)
		[openPanel setTitle: panelTitle];
	if (panelPrompt)
		[openPanel setPrompt: panelPrompt];

	if (fileTypes != nil)
		[openPanel setAllowedFileTypes: fileTypes];

	/* launch the panel */
	if ([openPanel runModal] == NSOKButton) {
		NSString *selectedURL = [[[openPanel URLs] firstObject] path];
		[selectedURL writeToFile:@"/dev/stdout" atomically:NO encoding: NSUTF8StringEncoding error: NULL];
		[application stop: nil];
		[pool drain];
		return 0;
	}

	[application stop: nil];
	[pool drain];
	return 1;
}