#import <Foundation/Foundation.h>
#import <AppKit/AppKit.h>

int main(int argc, char **argv)
{
	int ch;
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	NSApplication *application = [NSApplication sharedApplication];
	NSMutableArray *fileTypes = nil;
	NSSavePanel *savePanel = nil;
	NSString *appIconPath = nil;
	NSString *panelTitle = nil; 
	NSString *panelPrompt = nil; 

	while ((ch = getopt(argc, argv, "htpia")) != -1) {
		switch (ch) {
		case 'h':
			fputs( "-h help\n", stderr );
			fputs( "-t title\n", stderr );
			fputs( "-p prompt\n", stderr );
			fputs( "-i icon\n", stderr );
			fputs( "-a add file type\n", stderr );
			return 0;
			break;
		case 't':
			panelTitle = [NSString stringWithUTF8String: argv[optind]];
			optind++;
			break;
		case 'p':
			panelPrompt = [NSString stringWithUTF8String: argv[optind]];
			optind++;
			break;
		case 'i':
			appIconPath = [NSString stringWithUTF8String: argv[optind]];
			optind++;
			break;
		case 'a':
			if (fileTypes == nil)
				fileTypes = [NSMutableArray new];

			[fileTypes addObject: [NSString stringWithUTF8String: argv[optind]]];
			optind++;
			break;
		default:
			return 1;
		}
	}
	argc -= optind;
	argv += optind;

	if (appIconPath != nil) {
		NSImage *iconImage = [[NSImage alloc] initWithContentsOfFile: appIconPath];
		[application setApplicationIconImage: iconImage];
		[iconImage release];
	}

	/* needs to be inited after our app icon changes */
	savePanel = [NSSavePanel savePanel];

	/* options */
	if (fileTypes)
		[savePanel setAllowedFileTypes: fileTypes];
	if (panelTitle)
		[savePanel setTitle: panelTitle];
	if (panelPrompt)
		[savePanel setPrompt: panelPrompt];

	/* launch the panel */
	if ([savePanel runModal] == NSOKButton) {
		NSString *selectedURL = [[savePanel URL] path];
		[selectedURL writeToFile:@"/dev/stdout" atomically:NO encoding: NSUTF8StringEncoding error: NULL];
		[application stop: nil];
		[pool drain];
		return 0;
	}

	[application stop: nil];
	[pool drain];
	return 1;
}